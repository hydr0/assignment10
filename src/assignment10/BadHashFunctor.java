/*
 * CS2420-001 - Spring 2013
 *
 * Assignment 9:	HashTable
 * Due Date:		2013-03-28
 * Authors:			Victor Avila, Ben Ward
 */

package assignment10;

public class BadHashFunctor implements HashFunctor{

	@Override
	public int hash(String item) {
		return item.length();
	}

}
