/*
 * CS2420-001 - Spring 2013
 *
 * Assignment 9:	HashTable
 * Due Date:		2013-03-28
 * Authors:			Victor Avila, Ben Ward
 */

package assignment10;

import java.util.*;
import java.io.*;
import junit.framework.TestCase;

public class TestQuadProbeHashTable extends TestCase {

	private static ArrayList<String> set;

	public void testAdd() {
		QuadProbeHashTable c = new  QuadProbeHashTable(10, new MediocreHashFunctor());
		assertTrue(c.add("foo"));	
	}

	public void testAddDuplicate() {
		QuadProbeHashTable c = new  QuadProbeHashTable(10, new MediocreHashFunctor());
		c.add("foo");
		assertFalse(c.add("foo"));
	}

	public void testAddAll() throws FileNotFoundException {
		QuadProbeHashTable c = new  QuadProbeHashTable(100, new MediocreHashFunctor());
		fillSet();
		assertTrue(c.addAll(set));	
	}

	public void testClear() {
		QuadProbeHashTable c = new QuadProbeHashTable(10, new MediocreHashFunctor());
		c.add("bar");
		c.clear();
		assertEquals(0, c.size());
	}

	public void testContains() {
		QuadProbeHashTable c = new  QuadProbeHashTable(10, new MediocreHashFunctor());
		c.add("foo");
		assertTrue(c.contains("foo"));
	}
	
	public void testEmptContains() {
		QuadProbeHashTable c = new  QuadProbeHashTable(10, new MediocreHashFunctor());
		assertFalse(c.contains("foo"));
	}
	public void testContainsAll() throws FileNotFoundException {
		QuadProbeHashTable c = new  QuadProbeHashTable(50, new MediocreHashFunctor());
		fillSet();
		c.addAll(set);
		assertTrue(c.containsAll(set));		
	}

	public void testSize() throws FileNotFoundException {
		QuadProbeHashTable c = new  QuadProbeHashTable(100, new MediocreHashFunctor());
		fillSet();
		c.addAll(set);
		System.out.println(c.getCollision());
		assertEquals(set.size(), c.size());	
	}

	public void testEmpty() {
		QuadProbeHashTable c = new QuadProbeHashTable(10, new MediocreHashFunctor());
		c.add("bar");
		c.clear();
		assertTrue(c.isEmpty());
	}

	/**
	 * Helper used for adding a collection to the hash table.
	 * @throws FileNotFoundException
	 */
	public static void fillSet() throws FileNotFoundException {
		Scanner s = new Scanner(new File("moderate_word_list.txt"));
		set = new ArrayList<String>();
		while(s.hasNext()) {
			String a = s.next();
			if(!set.contains(a)) {
				set.add(a);
			}
		}
		s.close();
	}
}
