/*
 * CS2420-001 - Spring 2013
 *
 * Assignment 9:	HashTable
 * Due Date:		2013-03-28
 * Authors:			Victor Avila, Ben Ward
 */

package assignment10;

import junit.framework.TestCase;
import java.util.*;
import java.io.*;

public class TestChainingHash extends TestCase {

	private static ArrayList<String> set;

	public void testAdd() {
		ChainingHashTable c = new  ChainingHashTable(10, new GoodHashFunctor());
		assertTrue(c.add("foo"));	
	}

	public void testAddDuplicate() {
		ChainingHashTable c = new  ChainingHashTable(10, new GoodHashFunctor());
		c.add("foo");
		assertFalse(c.add("foo"));
	}

	public void testAddAll() throws FileNotFoundException {
		ChainingHashTable c = new  ChainingHashTable(100, new GoodHashFunctor());
		fillSet();
		assertTrue(c.addAll(set));	
	}

	public void testClear() {
		ChainingHashTable c = new ChainingHashTable(10, new GoodHashFunctor());
		c.add("bar");
		c.clear();
		assertEquals(0, c.size());
	}
	
	public void testEmptContains() {
		ChainingHashTable c = new  ChainingHashTable(10, new GoodHashFunctor());
		assertFalse(c.contains("foo"));
	}

	public void testContains() {
		ChainingHashTable c = new  ChainingHashTable(10, new GoodHashFunctor());
		c.add("foo");
		assertTrue(c.contains("foo"));
	}

	public void testContainsAll() throws FileNotFoundException {
		ChainingHashTable c = new  ChainingHashTable(50, new GoodHashFunctor());
		fillSet();
		c.addAll(set);
		System.out.println(c.getCollision());
		assertTrue(c.containsAll(set));		
	}

	public void testSize() throws FileNotFoundException {
		ChainingHashTable c = new  ChainingHashTable(100, new GoodHashFunctor());
		fillSet();
		c.addAll(set);
		assertEquals(set.size(), c.size());	
	}


	public void testEmpty() {
		ChainingHashTable c = new ChainingHashTable(10, new GoodHashFunctor());
		c.add("bar");
		c.clear();
		assertTrue(c.isEmpty());
	}

	/**
	 * Helper used for adding a collection to the hash table.
	 * @throws FileNotFoundException
	 */
	public static void fillSet() throws FileNotFoundException {
		Scanner s = new Scanner(new File("moderate_word_list.txt"));
		set = new ArrayList<String>();
		while(s.hasNext()) {
			String a = s.next();
			if(!set.contains(a)) {
				set.add(a);
			}
		}
		s.close();
	}
}
