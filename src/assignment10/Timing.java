/* CS2420-001 - Spring 2013
 *
 * Assignment 8:	TimeBinarySearchTree
 * Due Date:		2013-03-28
 * Authors:			Victor Avila, Ben Ward
 */

package assignment10;

import java.util.ArrayList;
import java.util.Random;
import java.util.TreeSet;

public class Timing{

	private static final int MAX_SIZE = 200000;
	private static final int INCREMENT = 5000;
	private static final int SEED = 123;
	private static Random RAND;

	public static void main(String[] args) {

		/*
		 *  QPHT.add timing.
		 *  Will also return the collision count per run.
		 */
		timeQAdd();
		
		/*
		 *  CHT.add timing
		 *  Will also return the collision count per run.
		 */
		timeCAdd();
	}

	/**
	 * Prints a pretty formatted header
	 * @param methodName the name of the method being tested
	 */
	private static void printHeader(String methodName) {
		System.out.printf("\n%10s: %-62s\n", "Timing", methodName);
		System.out.printf("%10s: %-62s\n", "Size", "Average Time (ns)");
		System.out.println("=============================");
	}

	/**
	 * Time QPHT.add
	 * This method works by looping through the code and creating the 
	 * QPHT which implements a different HashFunctor hbased on the condition.
	 * Adds elements of n/INCREMENT, TIMES_TO_LOOP times. 
	 *
	 * @param n The size of the lists to be created.
	 * @return The average time in ns.
	 */
	private static void timeQAdd() {
		final int TIMES_TO_LOOP = 1000;
		RAND = new Random(SEED);

		for(int k = 0; k < 3; k++) {

			if(k == 0){
				printHeader("Bad QPHT.add");
			}
			else if(k == 1) {
				printHeader("Mediocre QPHT.add");
			}
			else {
				printHeader("Good QPHT.add");
			}

			for(int n = INCREMENT; n <= MAX_SIZE; n += INCREMENT){

				long startTime;
				long midpointTime;
				long stopTime;

				QuadProbeHashTable test = null;


				if(k == 0){
					test = new QuadProbeHashTable(n, new BadHashFunctor());	
				}
				else if(k == 1) {
					test = new QuadProbeHashTable(n, new MediocreHashFunctor());	
				}
				else {
					test = new QuadProbeHashTable(n, new GoodHashFunctor());	
				}

				ArrayList<String> list = new ArrayList<String>(n);
				for(int i = 0; i < TIMES_TO_LOOP; i++) {
					list.add(randomString(RAND.nextInt(SEED)));
				}


				/*==========THIS IS THE TEST==========*/

				/* Wait a second. */
				startTime = System.nanoTime();
				while (System.nanoTime() - startTime < 100000000) {} // empty block

				/* Start the test */
				startTime = System.nanoTime();

				for(int i = 0; i < TIMES_TO_LOOP; i++) {
					test.add(list.get(i));
				}

				midpointTime = System.nanoTime();

				/*
				 * Run a loop to capture the cost of running the loop.
				 */
				for (int i = 0; i < TIMES_TO_LOOP; i++) {
					list.get(i);
				}

				stopTime = System.nanoTime();


				/*
				 * Compute the time, subtract the cost of running the loop
				 * from the cost of running the loop and computing square roots.
				 * Average it over the number of runs.
				 * Print the time as well as the size of the array
				 */
				System.out.println(/*"%10s: %-62s\n", n,*/ ((midpointTime - startTime)
						- (stopTime - midpointTime)) 
						/ TIMES_TO_LOOP);
				//System.out.println(test.getCollision());
			}
		}
	}
	
	/**
	 * Time CHT.add
	 * This method works by looping through the code and creating the 
	 * CHT which implements a different HashFunctor hbased on the condition.
	 * Adds elements of n/INCREMENT, TIMES_TO_LOOP times. 
	 *
	 * @param n The size of the lists to be created.
	 * @return The average time in ns.
	 */
	private static void timeCAdd() {
		final int TIMES_TO_LOOP = 1000;
		RAND = new Random(SEED);

		// Loops to make timing test
		for(int k = 0; k < 3; k++) {

			if(k == 0){
				printHeader("Bad CHT.add");
			}
			else if(k == 1) {
				printHeader("Mediocre CHT.add");
			}
			else {
				printHeader("Good CHT.add");
			}

			for(int n = INCREMENT; n <= MAX_SIZE; n += INCREMENT){

				long startTime;
				long midpointTime;
				long stopTime;

				// Init the hash table
				ChainingHashTable test = null;

				// Chaining hash table for bad, mediocre, and good functors

				if(k == 0){
					test = new ChainingHashTable(n, new BadHashFunctor());	
				}
				else if(k == 1) {
					test = new ChainingHashTable(n, new MediocreHashFunctor());	
				}
				else {
					test = new ChainingHashTable(n, new GoodHashFunctor());	
				}

				ArrayList<String> list = new ArrayList<String>(n);
				for(int i = 0; i < TIMES_TO_LOOP; i++) {
					list.add(randomString(RAND.nextInt(SEED)));
				}


				/*==========THIS IS THE TEST==========*/

				/* Wait a second. */
				startTime = System.nanoTime();
				while (System.nanoTime() - startTime < 100000000) {} // empty block

				/* Start the test */
				startTime = System.nanoTime();

				for(int i = 0; i < TIMES_TO_LOOP; i++) {
					test.add(list.get(i));
				}

				midpointTime = System.nanoTime();

				/*
				 * Run a loop to capture the cost of running the loop.
				 */
				for (int i = 0; i < TIMES_TO_LOOP; i++) {
					list.get(i);
				}

				stopTime = System.nanoTime();


				/*
				 * Compute the time, subtract the cost of running the loop
				 * from the cost of running the loop and computing square roots.
				 * Average it over the number of runs.
				 * Print the time as well as the size of the array
				 */
				System.out.println(/*"%10s: %-62s\n", n,*/ ((midpointTime - startTime)
						- (stopTime - midpointTime)) 
						/ TIMES_TO_LOOP);
				//System.out.println(test.getCollision());

			}
		}
	}

	

	/*
	 * This is code to generate a random string with a specified length
	 */
	public static String randomString(int length)
	{
		String retval = "";
		for(int i = 0; i < length; i++)
		{
			// ASCII values a-z,A-Z are contiguous (52 characters)
			retval += (char)('a' + (RAND.nextInt(26)));
		}
		return retval;
	}
}
