/*
 * CS2420-001 - Spring 2013
 *
 * Assignment 9:	HashTable
 * Due Date:		2013-03-28
 * Authors:			Victor Avila, Ben Ward
 */

package assignment10;

public class GoodHashFunctor implements HashFunctor {

	@Override
	public int hash(String item) {

		// This uses java's implementation of hashing a string
		int	n = item.length();
		int h = 0;
		for(int i = 0; i < item.length(); i++) {
			h += item.charAt(i)*Math.pow(31, n - i);
		}
		return Math.abs(h);
	}

}
