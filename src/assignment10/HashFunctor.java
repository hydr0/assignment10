/*
 * CS2420-001 - Spring 2013
 *
 * Assignment 9:	HashTable
 * Due Date:		2013-03-28
 * Authors:			Victor Avila, Ben Ward
 */

package assignment10;

/**
 * Serves as a guide for how to define a functor that contains a hashing
 * function for String items (i.e., the hash method).
 * 
 */
public interface HashFunctor {

  public int hash(String item);

}