/*
 * CS2420-001 - Spring 2013
 *
 * Assignment 9:	HashTable
 * Due Date:		2013-03-28
 * Authors:			Victor Avila, Ben Ward
 */

package assignment10;

import java.util.*;

public class QuadProbeHashTable implements Set<String> {

	private int size = 0;
	private String[] table;
	private HashFunctor f;
	private double LF = 0;
	private double cap = 0;
	private int collisions = 0;
	
	public static void main(String[] args) {
		QuadProbeHashTable n = new QuadProbeHashTable(10, new GoodHashFunctor());
		n.add("");
	}

	/**
	 * Creates a QPHT with the given parameters
	 * @param capacity
	 * @param functor
	 */
	public QuadProbeHashTable(int capacity, HashFunctor functor) {
		f = functor;
		if(isPrime(capacity)) {
			table = new String[(capacity)];
		}
		else {
			table = new String[(getPrime(capacity))]; 
		}
		cap = table.length;
	}

	/**
	 * Ensures that this set contains the specified item.
	 * 
	 * @param item -
	 *          the item whose presence is ensured in this set
	 * @return true if this set changed as a result of this method call (that is,
	 *         if the input item was actually inserted); otherwise, returns false
	 */
	@Override
	public boolean add(String item) {
		if(!contains(item)){
			if(LF >= 0.5) {
				rehash(table.length*2);
			}
			int i = f.hash(item) % table.length;
			if(table[i] == null) {
				table[i] = item;
				size++;
				LF = size/cap;
				return true;
			}
			else {
				for(int j = 1; j <= table.length; j++) {
					collisions++;
					if(table[(i + j*j)%table.length] == null){
						table[(i + j*j)%table.length] = item;
						size++;
						LF = size/cap;
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Ensures that this set contains all items in the specified collection.
	 * 
	 * @param items -
	 *          the collection of items whose presence is ensured in this set
	 * @return true if this set changed as a result of this method call (that is,
	 *         if any item in the input collection was actually inserted);
	 *         otherwise, returns false
	 */
	@Override
	public boolean addAll(Collection<? extends String> items) {
		boolean added = false;
		for(String s: items) {
			if(add(s)) {
				added = true;
			}
		}
		return added;
	}

	/**
	 * Removes all items from this set. The set will be empty after this method
	 * call.
	 */
	@Override
	public void clear() {
		table = new String[(int) cap];
		size = 0;
	}

	/**
	 * Determines if there is an item in this set that is equal to the specified
	 * item.
	 * 
	 * @param item -
	 *          the item sought in this set
	 * @return true if there is an item in this set that is equal to the input
	 *         item; otherwise, returns false
	 */
	@Override
	public boolean contains(String item) {
		int i = f.hash(item) % table.length;
		if(table[i] == null){
			return false;
		}

		if(table[i].equals(item)) {
			return true;
		}
		else {
			for(int j = 1; j <= table.length; j++) {
				if(table[(i + j*j)%table.length] == null){
					return false;
				}
				if(table[(i + j*j)%table.length].equals(item)){
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Determines if for each item in the specified collection, there is an item
	 * in this set that is equal to it.
	 * 
	 * @param items -
	 *          the collection of items sought in this set
	 * @return true if for each item in the specified collection, there is an item
	 *         in this set that is equal to it; otherwise, returns false
	 */
	@Override
	public boolean containsAll(Collection<? extends String> items) {
		for(String s: items) {
			if(!contains(s)){
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns true if this set contains no items.
	 */
	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Returns the number of items in this set.
	 */
	@Override
	public int size() {
		return size;
	}

	public int getCollision() {
		return collisions;
	}

	/**
	 * Checks if the parameter is a prime or not.
	 * @param capacity
	 * @return
	 */
	private static boolean isPrime(int n) {
		//check if n is a multiple of 2
		if (n%2==0) {
			return false;
		}
		//if not, then just check the odds
		for(int i=3;i*i<=n;i+=2) {
			if(n%i==0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns the next prime after the given parameter
	 * @param capacity
	 * @return
	 */
	private static int getPrime(int c) {
		for(int i = c+1; i <= 2*c; i++) {
			if(isPrime(i)){
				return i;
			}
		}
		return getPrime(2*c);
	}

	private void rehash(int s) {
		String[] temp = new String[getPrime(s)];

		for(int i = 0; i < table.length; i++){
			if(table[i] != null){
				int j = f.hash(table[i]) % temp.length;
				if(temp[j] == null){
					temp[j] = table[i];
				}
				else {
					for(int k = 1; k <= temp.length; k++) {
						if(temp[(j + k*k)%temp.length] != null){
							collisions++;
						}
						else{
							temp[(j + k*k)%temp.length] = table[i];
							break;
						}
					}
				}
			}
		}
		table = temp;
		cap = table.length;
	}
}

