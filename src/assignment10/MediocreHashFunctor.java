/*
 * CS2420-001 - Spring 2013
 *
 * Assignment 9:	HashTable
 * Due Date:		2013-03-28
 * Authors:			Victor Avila, Ben Ward
 */

package assignment10;

public class MediocreHashFunctor implements HashFunctor {

	@Override
	// Loops through the string and adds together all of the ascii values
	public int hash(String item) {
		int hash = 0;
		for(char c: item.toCharArray()){
			hash = hash + c;
		}
		return hash;
	}

}
