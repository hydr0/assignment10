/*
 * CS2420-001 - Spring 2013
 *
 * Assignment 9:	HashTable
 * Due Date:		2013-03-28
 * Authors:			Victor Avila, Ben Ward
 */

package assignment10;

import java.util.*;

public class ChainingHashTable implements Set<String> {


	private LinkedList<String>[] table;
	private HashFunctor f;
	private int numL = 0;
	private int collisionCount = 0;

	@SuppressWarnings("unchecked")
	public ChainingHashTable(int capacity, HashFunctor functor) {
		f = functor;
		table = (LinkedList<String>[])new LinkedList[capacity]; 
	}


	/**
	 * Ensures that this set contains the specified item.
	 * 
	 * @param item -
	 *          the item whose presence is ensured in this set
	 * @return true if this set changed as a result of this method call (that is,
	 *         if the input item was actually inserted); otherwise, returns false
	 */
	@Override
	public boolean add(String item) {
		if(!contains(item)){
			int i = f.hash(item)% table.length;
			if(table[i] == null) {
				table[i] = new LinkedList<String>();
				table[i].add(item);	
				numL++;
				return true;
			}
			else {
				table[i].add(item);
				return true;
			}
		}
		return false;
	}

	/**
	 * Ensures that this set contains all items in the specified collection.
	 * 
	 * @param items -
	 *          the collection of items whose presence is ensured in this set
	 * @return true if this set changed as a result of this method call (that is,
	 *         if any item in the input collection was actually inserted);
	 *         otherwise, returns false
	 */
	@Override
	public boolean addAll(Collection<? extends String> items) {
		boolean added = false;
		for(String s : items) {
			if(add(s)){
				added = true;
			}
		}
		return added;
	}

	/**
	 * Removes all items from this set. The set will be empty after this method
	 * call.
	 */
	@Override
	public void clear() {
		for(int i = 0; i < table.length; i++) {
			table[i] = null;
		}
	}

	/**
	 * Determines if there is an item in this set that is equal to the specified
	 * item.
	 * 
	 * @param item -
	 *          the item sought in this set
	 * @return true if there is an item in this set that is equal to the input
	 *         item; otherwise, returns false
	 */
	@Override
	public boolean contains(String item) {

		int i = f.hash(item)% table.length;
		if(table[i] == null) {
			return false;
		}
		else {
			for(String s : table[i]) {
				collisionCount++;
				if(s.equals(item)) {
					return true; 
				}
			}
		}

		return false;
	}

	/**
	 * Determines if for each item in the specified collection, there is an item
	 * in this set that is equal to it.
	 * 
	 * @param items -
	 *          the collection of items sought in this set
	 * @return true if for each item in the specified collection, there is an item
	 *         in this set that is equal to it; otherwise, returns false
	 */
	@Override
	public boolean containsAll(Collection<? extends String> items) {
		boolean cont = true;
		for(String s : items) {
			if(!contains(s)){
				cont = false;
			}
		}
		return cont;
	}

	/**
	 * Returns true if this set contains no items.
	 */
	@Override
	public boolean isEmpty() {
		boolean empty = true; 
		for(int i = 0; i < table.length; i++) {
			if(table[i] != null) {
				empty = false;
			}
		}
		return empty;
	}

	/**
	 * Returns the number of items in this set.
	 */
	@Override
	public int size() {
		int s = 0;
		for(int i = 0; i < table.length; i++) {
			if(table[i] != null){
				s += table[i].size();
			}
		}
		return s;
	}

	/** =========UNUSED. HERE FOR DEMO ONLY============
	 * Computes the load factor as a function of the sum of the elements in
	 * each list divided by the size of the table.
	 * @return
	 */
	public double lFact() {
		double fac = 0;
		for(LinkedList<String> l: table) {
			if(!(l == null)){
				fac += l.size();
			}
		}
		return fac/table.length;
	}
	
	public int getCollision() {
		return collisionCount;
	}
	
	/**=========UNUSED. HERE FOR DEMO ONLY============
	 * Rehash function.
	 * was originally implemented
	 * @param s
	 */
	@SuppressWarnings("unchecked")
	private void rehash(int s) {
		LinkedList<String>[] temp = (LinkedList<String>[]) new LinkedList[s];
		for(int i = 0; i < table.length; i++){
			if(table[i] != null){
				for(String e: table[i]){
					int j = f.hash(e) % temp.length;
					if(temp[j] == null) {
						temp[j] = new LinkedList<String>();
						temp[j].add(e);
					}
					else{
						temp[j].add(e);
						collisionCount++;
					}
				}
			}
		}
		table = temp;
	}
}
